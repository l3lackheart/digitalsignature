﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitalSignature
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void buttonHash_Click(object sender, EventArgs e)
        {
            Hash hashForm = new Hash();
            hashForm.Show();
        }

        private void buttonGenerateKey_Click(object sender, EventArgs e)
        {
            GenerateKey generateKeyForm = new GenerateKey();
            generateKeyForm.Show();
        }

        private void buttonEncode_Click(object sender, EventArgs e)
        {
            Cryptography cryptoForm = new Cryptography();
            cryptoForm.Show();
        }
    }
}
