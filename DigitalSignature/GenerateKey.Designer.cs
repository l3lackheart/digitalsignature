﻿namespace DigitalSignature
{
    partial class GenerateKey
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNewKey = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textEncryptKey = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textDecryptKey = new System.Windows.Forms.TextBox();
            this.textModule = new System.Windows.Forms.TextBox();
            this.buttonCopyPublicKey = new System.Windows.Forms.Button();
            this.buttonPrivateKey = new System.Windows.Forms.Button();
            this.buttonCopyModule = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnNewKey
            // 
            this.btnNewKey.Location = new System.Drawing.Point(12, 12);
            this.btnNewKey.Name = "btnNewKey";
            this.btnNewKey.Size = new System.Drawing.Size(117, 29);
            this.btnNewKey.TabIndex = 1;
            this.btnNewKey.Text = "Sinh bộ khóa mới";
            this.btnNewKey.UseVisualStyleBackColor = true;
            this.btnNewKey.Click += new System.EventHandler(this.btnNewKey_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Khóa công khai";
            this.label1.UseCompatibleTextRendering = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Khóa bí mật";
            // 
            // textEncryptKey
            // 
            this.textEncryptKey.Enabled = false;
            this.textEncryptKey.Location = new System.Drawing.Point(29, 96);
            this.textEncryptKey.Name = "textEncryptKey";
            this.textEncryptKey.ReadOnly = true;
            this.textEncryptKey.Size = new System.Drawing.Size(100, 20);
            this.textEncryptKey.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 251);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Module";
            // 
            // textDecryptKey
            // 
            this.textDecryptKey.Enabled = false;
            this.textDecryptKey.Location = new System.Drawing.Point(29, 181);
            this.textDecryptKey.Name = "textDecryptKey";
            this.textDecryptKey.ReadOnly = true;
            this.textDecryptKey.Size = new System.Drawing.Size(323, 20);
            this.textDecryptKey.TabIndex = 3;
            // 
            // textModule
            // 
            this.textModule.Enabled = false;
            this.textModule.Location = new System.Drawing.Point(29, 267);
            this.textModule.Name = "textModule";
            this.textModule.ReadOnly = true;
            this.textModule.Size = new System.Drawing.Size(323, 20);
            this.textModule.TabIndex = 4;
            // 
            // buttonCopyPublicKey
            // 
            this.buttonCopyPublicKey.Enabled = false;
            this.buttonCopyPublicKey.Location = new System.Drawing.Point(29, 123);
            this.buttonCopyPublicKey.Name = "buttonCopyPublicKey";
            this.buttonCopyPublicKey.Size = new System.Drawing.Size(75, 23);
            this.buttonCopyPublicKey.TabIndex = 5;
            this.buttonCopyPublicKey.Text = "Sao chép";
            this.buttonCopyPublicKey.UseVisualStyleBackColor = true;
            this.buttonCopyPublicKey.Click += new System.EventHandler(this.buttonCopyPublicKey_Click);
            // 
            // buttonPrivateKey
            // 
            this.buttonPrivateKey.Enabled = false;
            this.buttonPrivateKey.Location = new System.Drawing.Point(29, 208);
            this.buttonPrivateKey.Name = "buttonPrivateKey";
            this.buttonPrivateKey.Size = new System.Drawing.Size(75, 23);
            this.buttonPrivateKey.TabIndex = 6;
            this.buttonPrivateKey.Text = "Sao chép";
            this.buttonPrivateKey.UseVisualStyleBackColor = true;
            this.buttonPrivateKey.Click += new System.EventHandler(this.buttonPrivateKey_Click);
            // 
            // buttonCopyModule
            // 
            this.buttonCopyModule.Enabled = false;
            this.buttonCopyModule.Location = new System.Drawing.Point(29, 294);
            this.buttonCopyModule.Name = "buttonCopyModule";
            this.buttonCopyModule.Size = new System.Drawing.Size(75, 23);
            this.buttonCopyModule.TabIndex = 7;
            this.buttonCopyModule.Text = "Sao chép";
            this.buttonCopyModule.UseVisualStyleBackColor = true;
            this.buttonCopyModule.Click += new System.EventHandler(this.buttonCopyModule_Click);
            // 
            // GenerateKey
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 361);
            this.Controls.Add(this.buttonCopyModule);
            this.Controls.Add(this.buttonPrivateKey);
            this.Controls.Add(this.buttonCopyPublicKey);
            this.Controls.Add(this.textModule);
            this.Controls.Add(this.textDecryptKey);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textEncryptKey);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnNewKey);
            this.Name = "GenerateKey";
            this.Text = "GenerateKey";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNewKey;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textEncryptKey;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textDecryptKey;
        private System.Windows.Forms.TextBox textModule;
        private System.Windows.Forms.Button buttonCopyPublicKey;
        private System.Windows.Forms.Button buttonPrivateKey;
        private System.Windows.Forms.Button buttonCopyModule;
    }
}