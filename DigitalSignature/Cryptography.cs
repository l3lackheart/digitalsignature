﻿using System;
using System.Numerics;
using System.Text;
using System.Windows.Forms;

namespace DigitalSignature
{
    public partial class Cryptography : Form
    {
        public Cryptography()
        {
            InitializeComponent();
        }

        private void buttonEncrypt_Click(object sender, EventArgs e)
        {                      
            try
            {
                // bind string into an array of bytes
                string plainText = textPlainText.Text;
                byte[] toNumber = Encoding.UTF8.GetBytes(textPlainText.Text);

                int length = toNumber.Length;

                // innate an array to store result
                BigInteger[] resultArray = new BigInteger[length];
                BigInteger encryptKey = BigInteger.Parse(textEncryptKey.Text);
                BigInteger module = BigInteger.Parse(textModuleEncryptMethod.Text);
                for (int i = 0; i < length; i++)
                {
                    resultArray[i] = BigInteger.ModPow(toNumber[i], encryptKey, module);
                }

                textEncryptedTextEncryptMethod.Text = string.Join(" ", resultArray); // left a space (#32) between each element
                textEncryptedTextEncryptMethod.Enabled = true;
            }            
            catch
            {
                MessageBox.Show("Không thể mã hóa dữ liệu!", "Thông báo", MessageBoxButtons.OK);
            }
        }

        private void buttonDecrypt_Click(object sender, EventArgs e)
        {           
            try
            {
                string encrypted = textEncryptedTextDecryptMethod.Text;
                string[] encryptedData = encrypted.Split(' ');
                byte[] resultArray = new byte[encryptedData.Length];
                BigInteger decryptKey = BigInteger.Parse(textDecryptKey.Text);
                BigInteger module = BigInteger.Parse(textModuleDecryptMethod.Text);

                for (int i = 0; i < encryptedData.Length; i++)
                {
                    BigInteger token = BigInteger.Parse(encryptedData[i]);
                    token = BigInteger.ModPow(token, decryptKey, module);
                    resultArray[i] = (byte)token;
                }
                
                textDecryptedText.Text = Encoding.UTF8.GetString(resultArray);
                textDecryptedText.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Không thể giải mã dữ liệu!", "Thông báo", MessageBoxButtons.OK);
            }           
        }

        private void buttonCompare_Click(object sender, EventArgs e)
        {
            string comparer1 = textCompare1.Text;
            string comparer2 = textCompare2.Text;

            if (comparer1 == comparer2)
            {
                pictureCompareResult.Image = Properties.Resources.tick;
                pictureCompareResult.SizeMode = PictureBoxSizeMode.AutoSize;
            }
            else
            {
                pictureCompareResult.Image = Properties.Resources.cross;
                pictureCompareResult.SizeMode = PictureBoxSizeMode.AutoSize;
            }
        }

        private void buttonToLower1_Click(object sender, EventArgs e)
        {
            textCompare1.Text = textCompare1.Text.ToLower();
        }

        private void buttonToLower2_Click(object sender, EventArgs e)
        {
            textCompare2.Text = textCompare2.Text.ToLower();
        }

        private void buttonCopyEncrypt_Click(object sender, EventArgs e)
        {
            if (textEncryptedTextEncryptMethod.Text != "")
            {
                Clipboard.SetText(textEncryptedTextEncryptMethod.Text);
                MessageBox.Show("Đã sao chép vào bộ nhớ đệm", "Thông báo", MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show("Không có gì để sao chép!", "Thông báo", MessageBoxButtons.OK);
            }
        }

        private void buttonCopyDecrypt_Click(object sender, EventArgs e)
        {
            if (textEncryptedTextDecryptMethod.Text != "")
            {
                Clipboard.SetText(textDecryptedText.Text);
                MessageBox.Show("Đã sao chép vào bộ nhớ đệm", "Thông báo", MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show("Không có gì để sao chép!", "Thông báo", MessageBoxButtons.OK);
            }
        }

        private void buttonPasteEncrypt_Click(object sender, EventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                textPlainText.Text += Clipboard.GetText();
            }
            else
            {
                MessageBox.Show("Không có gì để dán!", "Thông báo", MessageBoxButtons.OK);
            }
        }

        private void buttonPasteEncryptKey_Click(object sender, EventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                textEncryptKey.Text += Clipboard.GetText();
            }
            else
            {
                MessageBox.Show("Không có gì để dán!", "Thông báo", MessageBoxButtons.OK);
            }
        }

        private void buttonPasteModuleEncrypt_Click(object sender, EventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                textModuleEncryptMethod.Text += Clipboard.GetText();
            }
            else
            {
                MessageBox.Show("Không có gì để dán!", "Thông báo", MessageBoxButtons.OK);
            }
        }

        private void buttonPasteDecrypt_Click(object sender, EventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                textEncryptedTextDecryptMethod.Text += Clipboard.GetText();
            }
            else
            {
                MessageBox.Show("Không có gì để dán!", "Thông báo", MessageBoxButtons.OK);
            }
        }

        private void buttonPasteDecryptKey_Click(object sender, EventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                textDecryptKey.Text += Clipboard.GetText();
            }
            else
            {
                MessageBox.Show("Không có gì để dán!", "Thông báo", MessageBoxButtons.OK);
            }
        }

        private void buttonPasteModuleDecrypt_Click(object sender, EventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                textModuleDecryptMethod.Text += Clipboard.GetText();
            }
            else
            {
                MessageBox.Show("Không có gì để dán!", "Thông báo", MessageBoxButtons.OK);
            }
        }

        private void buttonPasteCompare1_Click(object sender, EventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                textCompare1.Text += Clipboard.GetText();
            }
            else
            {
                MessageBox.Show("Không có gì để dán!", "Thông báo", MessageBoxButtons.OK);
            }
        }

        private void buttonPasteCompare2_Click(object sender, EventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                textCompare2.Text += Clipboard.GetText();
            }
            else
            {
                MessageBox.Show("Không có gì để dán!", "Thông báo", MessageBoxButtons.OK);
            }
        }
    }
}
