﻿namespace DigitalSignature
{
    partial class Cryptography
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupEncrypt = new System.Windows.Forms.GroupBox();
            this.buttonPasteModuleEncrypt = new System.Windows.Forms.Button();
            this.buttonPasteEncrypt = new System.Windows.Forms.Button();
            this.buttonPasteEncryptKey = new System.Windows.Forms.Button();
            this.buttonCopyEncrypt = new System.Windows.Forms.Button();
            this.buttonEncrypt = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textModuleEncryptMethod = new System.Windows.Forms.TextBox();
            this.textEncryptKey = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textEncryptedTextEncryptMethod = new System.Windows.Forms.RichTextBox();
            this.textPlainText = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pictureCompareResult = new System.Windows.Forms.PictureBox();
            this.buttonPasteCompare2 = new System.Windows.Forms.Button();
            this.buttonPasteCompare1 = new System.Windows.Forms.Button();
            this.buttonToLower1 = new System.Windows.Forms.Button();
            this.buttonToLower2 = new System.Windows.Forms.Button();
            this.labelCompareResult = new System.Windows.Forms.Label();
            this.buttonCompare = new System.Windows.Forms.Button();
            this.textCompare2 = new System.Windows.Forms.TextBox();
            this.textCompare1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupDecrypt = new System.Windows.Forms.GroupBox();
            this.buttonPasteModuleDecrypt = new System.Windows.Forms.Button();
            this.buttonCopyDecrypt = new System.Windows.Forms.Button();
            this.buttonPasteDecrypt = new System.Windows.Forms.Button();
            this.buttonPasteDecryptKey = new System.Windows.Forms.Button();
            this.buttonDecrypt = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textModuleDecryptMethod = new System.Windows.Forms.TextBox();
            this.textDecryptKey = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textDecryptedText = new System.Windows.Forms.RichTextBox();
            this.textEncryptedTextDecryptMethod = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupEncrypt.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCompareResult)).BeginInit();
            this.groupDecrypt.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupEncrypt
            // 
            this.groupEncrypt.Controls.Add(this.buttonPasteModuleEncrypt);
            this.groupEncrypt.Controls.Add(this.buttonPasteEncrypt);
            this.groupEncrypt.Controls.Add(this.buttonPasteEncryptKey);
            this.groupEncrypt.Controls.Add(this.buttonCopyEncrypt);
            this.groupEncrypt.Controls.Add(this.buttonEncrypt);
            this.groupEncrypt.Controls.Add(this.label7);
            this.groupEncrypt.Controls.Add(this.label6);
            this.groupEncrypt.Controls.Add(this.textModuleEncryptMethod);
            this.groupEncrypt.Controls.Add(this.textEncryptKey);
            this.groupEncrypt.Controls.Add(this.label5);
            this.groupEncrypt.Controls.Add(this.textEncryptedTextEncryptMethod);
            this.groupEncrypt.Controls.Add(this.textPlainText);
            this.groupEncrypt.Controls.Add(this.label1);
            this.groupEncrypt.Location = new System.Drawing.Point(12, 12);
            this.groupEncrypt.Name = "groupEncrypt";
            this.groupEncrypt.Size = new System.Drawing.Size(325, 380);
            this.groupEncrypt.TabIndex = 0;
            this.groupEncrypt.TabStop = false;
            this.groupEncrypt.Text = "Mã hóa";
            // 
            // buttonPasteModuleEncrypt
            // 
            this.buttonPasteModuleEncrypt.Location = new System.Drawing.Point(251, 206);
            this.buttonPasteModuleEncrypt.Name = "buttonPasteModuleEncrypt";
            this.buttonPasteModuleEncrypt.Size = new System.Drawing.Size(65, 22);
            this.buttonPasteModuleEncrypt.TabIndex = 10;
            this.buttonPasteModuleEncrypt.Text = "Dán";
            this.buttonPasteModuleEncrypt.UseVisualStyleBackColor = true;
            this.buttonPasteModuleEncrypt.Click += new System.EventHandler(this.buttonPasteModuleEncrypt_Click);
            // 
            // buttonPasteEncrypt
            // 
            this.buttonPasteEncrypt.Location = new System.Drawing.Point(251, 121);
            this.buttonPasteEncrypt.Name = "buttonPasteEncrypt";
            this.buttonPasteEncrypt.Size = new System.Drawing.Size(65, 22);
            this.buttonPasteEncrypt.TabIndex = 10;
            this.buttonPasteEncrypt.Text = "Dán";
            this.buttonPasteEncrypt.UseVisualStyleBackColor = true;
            this.buttonPasteEncrypt.Click += new System.EventHandler(this.buttonPasteEncrypt_Click);
            // 
            // buttonPasteEncryptKey
            // 
            this.buttonPasteEncryptKey.Location = new System.Drawing.Point(251, 167);
            this.buttonPasteEncryptKey.Name = "buttonPasteEncryptKey";
            this.buttonPasteEncryptKey.Size = new System.Drawing.Size(65, 22);
            this.buttonPasteEncryptKey.TabIndex = 10;
            this.buttonPasteEncryptKey.Text = "Dán";
            this.buttonPasteEncryptKey.UseVisualStyleBackColor = true;
            this.buttonPasteEncryptKey.Click += new System.EventHandler(this.buttonPasteEncryptKey_Click);
            // 
            // buttonCopyEncrypt
            // 
            this.buttonCopyEncrypt.Location = new System.Drawing.Point(251, 323);
            this.buttonCopyEncrypt.Name = "buttonCopyEncrypt";
            this.buttonCopyEncrypt.Size = new System.Drawing.Size(65, 22);
            this.buttonCopyEncrypt.TabIndex = 9;
            this.buttonCopyEncrypt.Text = "Sao chép";
            this.buttonCopyEncrypt.UseVisualStyleBackColor = true;
            this.buttonCopyEncrypt.Click += new System.EventHandler(this.buttonCopyEncrypt_Click);
            // 
            // buttonEncrypt
            // 
            this.buttonEncrypt.Location = new System.Drawing.Point(128, 352);
            this.buttonEncrypt.Name = "buttonEncrypt";
            this.buttonEncrypt.Size = new System.Drawing.Size(75, 23);
            this.buttonEncrypt.TabIndex = 8;
            this.buttonEncrypt.Text = "Mã hóa";
            this.buttonEncrypt.UseVisualStyleBackColor = true;
            this.buttonEncrypt.Click += new System.EventHandler(this.buttonEncrypt_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 230);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Chuỗi đã mã hóa";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 191);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Module";
            // 
            // textModuleEncryptMethod
            // 
            this.textModuleEncryptMethod.Location = new System.Drawing.Point(9, 207);
            this.textModuleEncryptMethod.Name = "textModuleEncryptMethod";
            this.textModuleEncryptMethod.Size = new System.Drawing.Size(305, 20);
            this.textModuleEncryptMethod.TabIndex = 4;
            // 
            // textEncryptKey
            // 
            this.textEncryptKey.Location = new System.Drawing.Point(9, 168);
            this.textEncryptKey.Name = "textEncryptKey";
            this.textEncryptKey.Size = new System.Drawing.Size(305, 20);
            this.textEncryptKey.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Khóa mã hóa";
            // 
            // textEncryptedTextEncryptMethod
            // 
            this.textEncryptedTextEncryptMethod.Enabled = false;
            this.textEncryptedTextEncryptMethod.Location = new System.Drawing.Point(6, 246);
            this.textEncryptedTextEncryptMethod.Name = "textEncryptedTextEncryptMethod";
            this.textEncryptedTextEncryptMethod.ReadOnly = true;
            this.textEncryptedTextEncryptMethod.Size = new System.Drawing.Size(311, 100);
            this.textEncryptedTextEncryptMethod.TabIndex = 1;
            this.textEncryptedTextEncryptMethod.Text = "";
            // 
            // textPlainText
            // 
            this.textPlainText.Location = new System.Drawing.Point(9, 44);
            this.textPlainText.Name = "textPlainText";
            this.textPlainText.Size = new System.Drawing.Size(308, 100);
            this.textPlainText.TabIndex = 1;
            this.textPlainText.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Chuỗi cần mã hóa";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.pictureCompareResult);
            this.groupBox3.Controls.Add(this.buttonPasteCompare2);
            this.groupBox3.Controls.Add(this.buttonPasteCompare1);
            this.groupBox3.Controls.Add(this.buttonToLower1);
            this.groupBox3.Controls.Add(this.buttonToLower2);
            this.groupBox3.Controls.Add(this.labelCompareResult);
            this.groupBox3.Controls.Add(this.buttonCompare);
            this.groupBox3.Controls.Add(this.textCompare2);
            this.groupBox3.Controls.Add(this.textCompare1);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(12, 399);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(660, 100);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "So sánh";
            // 
            // pictureCompareResult
            // 
            this.pictureCompareResult.Location = new System.Drawing.Point(582, 17);
            this.pictureCompareResult.Name = "pictureCompareResult";
            this.pictureCompareResult.Size = new System.Drawing.Size(70, 70);
            this.pictureCompareResult.TabIndex = 6;
            this.pictureCompareResult.TabStop = false;
            // 
            // buttonPasteCompare2
            // 
            this.buttonPasteCompare2.Location = new System.Drawing.Point(428, 46);
            this.buttonPasteCompare2.Name = "buttonPasteCompare2";
            this.buttonPasteCompare2.Size = new System.Drawing.Size(65, 22);
            this.buttonPasteCompare2.TabIndex = 10;
            this.buttonPasteCompare2.Text = "Dán";
            this.buttonPasteCompare2.UseVisualStyleBackColor = true;
            this.buttonPasteCompare2.Click += new System.EventHandler(this.buttonPasteCompare2_Click);
            // 
            // buttonPasteCompare1
            // 
            this.buttonPasteCompare1.Location = new System.Drawing.Point(428, 23);
            this.buttonPasteCompare1.Name = "buttonPasteCompare1";
            this.buttonPasteCompare1.Size = new System.Drawing.Size(65, 22);
            this.buttonPasteCompare1.TabIndex = 10;
            this.buttonPasteCompare1.Text = "Dán";
            this.buttonPasteCompare1.UseVisualStyleBackColor = true;
            this.buttonPasteCompare1.Click += new System.EventHandler(this.buttonPasteCompare1_Click);
            // 
            // buttonToLower1
            // 
            this.buttonToLower1.Location = new System.Drawing.Point(499, 23);
            this.buttonToLower1.Name = "buttonToLower1";
            this.buttonToLower1.Size = new System.Drawing.Size(75, 20);
            this.buttonToLower1.TabIndex = 5;
            this.buttonToLower1.Text = "Xóa in hoa";
            this.buttonToLower1.UseVisualStyleBackColor = true;
            this.buttonToLower1.Click += new System.EventHandler(this.buttonToLower1_Click);
            // 
            // buttonToLower2
            // 
            this.buttonToLower2.Location = new System.Drawing.Point(499, 47);
            this.buttonToLower2.Name = "buttonToLower2";
            this.buttonToLower2.Size = new System.Drawing.Size(75, 20);
            this.buttonToLower2.TabIndex = 5;
            this.buttonToLower2.Text = "Xóa in hoa";
            this.buttonToLower2.UseVisualStyleBackColor = true;
            this.buttonToLower2.Click += new System.EventHandler(this.buttonToLower2_Click);
            // 
            // labelCompareResult
            // 
            this.labelCompareResult.AutoSize = true;
            this.labelCompareResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompareResult.Location = new System.Drawing.Point(565, 36);
            this.labelCompareResult.Name = "labelCompareResult";
            this.labelCompareResult.Size = new System.Drawing.Size(0, 16);
            this.labelCompareResult.TabIndex = 4;
            // 
            // buttonCompare
            // 
            this.buttonCompare.Location = new System.Drawing.Point(59, 73);
            this.buttonCompare.Name = "buttonCompare";
            this.buttonCompare.Size = new System.Drawing.Size(75, 23);
            this.buttonCompare.TabIndex = 3;
            this.buttonCompare.Text = "So sánh";
            this.buttonCompare.UseVisualStyleBackColor = true;
            this.buttonCompare.Click += new System.EventHandler(this.buttonCompare_Click);
            // 
            // textCompare2
            // 
            this.textCompare2.Location = new System.Drawing.Point(59, 47);
            this.textCompare2.Name = "textCompare2";
            this.textCompare2.Size = new System.Drawing.Size(434, 20);
            this.textCompare2.TabIndex = 2;
            // 
            // textCompare1
            // 
            this.textCompare1.Location = new System.Drawing.Point(59, 24);
            this.textCompare1.Name = "textCompare1";
            this.textCompare1.Size = new System.Drawing.Size(434, 20);
            this.textCompare1.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Chuỗi 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Chuỗi 1";
            // 
            // groupDecrypt
            // 
            this.groupDecrypt.Controls.Add(this.buttonPasteModuleDecrypt);
            this.groupDecrypt.Controls.Add(this.buttonCopyDecrypt);
            this.groupDecrypt.Controls.Add(this.buttonPasteDecrypt);
            this.groupDecrypt.Controls.Add(this.buttonPasteDecryptKey);
            this.groupDecrypt.Controls.Add(this.buttonDecrypt);
            this.groupDecrypt.Controls.Add(this.label2);
            this.groupDecrypt.Controls.Add(this.label8);
            this.groupDecrypt.Controls.Add(this.textModuleDecryptMethod);
            this.groupDecrypt.Controls.Add(this.textDecryptKey);
            this.groupDecrypt.Controls.Add(this.label9);
            this.groupDecrypt.Controls.Add(this.textDecryptedText);
            this.groupDecrypt.Controls.Add(this.textEncryptedTextDecryptMethod);
            this.groupDecrypt.Controls.Add(this.label10);
            this.groupDecrypt.Location = new System.Drawing.Point(347, 11);
            this.groupDecrypt.Name = "groupDecrypt";
            this.groupDecrypt.Size = new System.Drawing.Size(325, 380);
            this.groupDecrypt.TabIndex = 0;
            this.groupDecrypt.TabStop = false;
            this.groupDecrypt.Text = "Giải mã";
            // 
            // buttonPasteModuleDecrypt
            // 
            this.buttonPasteModuleDecrypt.Location = new System.Drawing.Point(252, 206);
            this.buttonPasteModuleDecrypt.Name = "buttonPasteModuleDecrypt";
            this.buttonPasteModuleDecrypt.Size = new System.Drawing.Size(65, 22);
            this.buttonPasteModuleDecrypt.TabIndex = 10;
            this.buttonPasteModuleDecrypt.Text = "Dán";
            this.buttonPasteModuleDecrypt.UseVisualStyleBackColor = true;
            this.buttonPasteModuleDecrypt.Click += new System.EventHandler(this.buttonPasteModuleDecrypt_Click);
            // 
            // buttonCopyDecrypt
            // 
            this.buttonCopyDecrypt.Location = new System.Drawing.Point(252, 324);
            this.buttonCopyDecrypt.Name = "buttonCopyDecrypt";
            this.buttonCopyDecrypt.Size = new System.Drawing.Size(65, 22);
            this.buttonCopyDecrypt.TabIndex = 9;
            this.buttonCopyDecrypt.Text = "Sao chép";
            this.buttonCopyDecrypt.UseVisualStyleBackColor = true;
            this.buttonCopyDecrypt.Click += new System.EventHandler(this.buttonCopyDecrypt_Click);
            // 
            // buttonPasteDecrypt
            // 
            this.buttonPasteDecrypt.Location = new System.Drawing.Point(252, 122);
            this.buttonPasteDecrypt.Name = "buttonPasteDecrypt";
            this.buttonPasteDecrypt.Size = new System.Drawing.Size(65, 22);
            this.buttonPasteDecrypt.TabIndex = 10;
            this.buttonPasteDecrypt.Text = "Dán";
            this.buttonPasteDecrypt.UseVisualStyleBackColor = true;
            this.buttonPasteDecrypt.Click += new System.EventHandler(this.buttonPasteDecrypt_Click);
            // 
            // buttonPasteDecryptKey
            // 
            this.buttonPasteDecryptKey.Location = new System.Drawing.Point(252, 167);
            this.buttonPasteDecryptKey.Name = "buttonPasteDecryptKey";
            this.buttonPasteDecryptKey.Size = new System.Drawing.Size(65, 22);
            this.buttonPasteDecryptKey.TabIndex = 10;
            this.buttonPasteDecryptKey.Text = "Dán";
            this.buttonPasteDecryptKey.UseVisualStyleBackColor = true;
            this.buttonPasteDecryptKey.Click += new System.EventHandler(this.buttonPasteDecryptKey_Click);
            // 
            // buttonDecrypt
            // 
            this.buttonDecrypt.Location = new System.Drawing.Point(137, 352);
            this.buttonDecrypt.Name = "buttonDecrypt";
            this.buttonDecrypt.Size = new System.Drawing.Size(75, 23);
            this.buttonDecrypt.TabIndex = 8;
            this.buttonDecrypt.Text = "Giải mã";
            this.buttonDecrypt.UseVisualStyleBackColor = true;
            this.buttonDecrypt.Click += new System.EventHandler(this.buttonDecrypt_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 230);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Chuỗi đã giải mã";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 191);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Module";
            // 
            // textModuleDecryptMethod
            // 
            this.textModuleDecryptMethod.Location = new System.Drawing.Point(9, 207);
            this.textModuleDecryptMethod.Name = "textModuleDecryptMethod";
            this.textModuleDecryptMethod.Size = new System.Drawing.Size(305, 20);
            this.textModuleDecryptMethod.TabIndex = 4;
            // 
            // textDecryptKey
            // 
            this.textDecryptKey.Location = new System.Drawing.Point(9, 168);
            this.textDecryptKey.Name = "textDecryptKey";
            this.textDecryptKey.Size = new System.Drawing.Size(305, 20);
            this.textDecryptKey.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 152);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Khóa giải mã";
            // 
            // textDecryptedText
            // 
            this.textDecryptedText.Enabled = false;
            this.textDecryptedText.Location = new System.Drawing.Point(6, 246);
            this.textDecryptedText.Name = "textDecryptedText";
            this.textDecryptedText.ReadOnly = true;
            this.textDecryptedText.Size = new System.Drawing.Size(311, 100);
            this.textDecryptedText.TabIndex = 1;
            this.textDecryptedText.Text = "";
            // 
            // textEncryptedTextDecryptMethod
            // 
            this.textEncryptedTextDecryptMethod.Location = new System.Drawing.Point(9, 44);
            this.textEncryptedTextDecryptMethod.Name = "textEncryptedTextDecryptMethod";
            this.textEncryptedTextDecryptMethod.Size = new System.Drawing.Size(308, 100);
            this.textEncryptedTextDecryptMethod.TabIndex = 1;
            this.textEncryptedTextDecryptMethod.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Chuỗi cần giải mã";
            // 
            // Cryptography
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 511);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupDecrypt);
            this.Controls.Add(this.groupEncrypt);
            this.Name = "Cryptography";
            this.Text = "Cryptography";
            this.groupEncrypt.ResumeLayout(false);
            this.groupEncrypt.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCompareResult)).EndInit();
            this.groupDecrypt.ResumeLayout(false);
            this.groupDecrypt.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupEncrypt;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textModuleEncryptMethod;
        private System.Windows.Forms.TextBox textEncryptKey;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox textPlainText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textCompare2;
        private System.Windows.Forms.TextBox textCompare1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonEncrypt;
        private System.Windows.Forms.Button buttonCompare;
        private System.Windows.Forms.RichTextBox textEncryptedTextEncryptMethod;
        private System.Windows.Forms.GroupBox groupDecrypt;
        private System.Windows.Forms.Button buttonDecrypt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textModuleDecryptMethod;
        private System.Windows.Forms.TextBox textDecryptKey;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RichTextBox textDecryptedText;
        private System.Windows.Forms.RichTextBox textEncryptedTextDecryptMethod;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelCompareResult;
        private System.Windows.Forms.Button buttonToLower1;
        private System.Windows.Forms.Button buttonToLower2;
        private System.Windows.Forms.PictureBox pictureCompareResult;
        private System.Windows.Forms.Button buttonCopyEncrypt;
        private System.Windows.Forms.Button buttonCopyDecrypt;
        private System.Windows.Forms.Button buttonPasteModuleEncrypt;
        private System.Windows.Forms.Button buttonPasteEncrypt;
        private System.Windows.Forms.Button buttonPasteEncryptKey;
        private System.Windows.Forms.Button buttonPasteModuleDecrypt;
        private System.Windows.Forms.Button buttonPasteDecrypt;
        private System.Windows.Forms.Button buttonPasteDecryptKey;
        private System.Windows.Forms.Button buttonPasteCompare2;
        private System.Windows.Forms.Button buttonPasteCompare1;
    }
}