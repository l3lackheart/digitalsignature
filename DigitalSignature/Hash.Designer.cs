﻿namespace DigitalSignature
{
    partial class Hash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUploadFile = new System.Windows.Forms.Button();
            this.labelFileUploaded = new System.Windows.Forms.TextBox();
            this.textPlainText = new System.Windows.Forms.RichTextBox();
            this.radioBtnFile = new System.Windows.Forms.RadioButton();
            this.radioBtnText = new System.Windows.Forms.RadioButton();
            this.btnHashMD5 = new System.Windows.Forms.Button();
            this.btnHashSHA1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textHashResult = new System.Windows.Forms.RichTextBox();
            this.fileUpload = new System.Windows.Forms.OpenFileDialog();
            this.buttonPaste = new System.Windows.Forms.Button();
            this.buttonCopy = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnUploadFile
            // 
            this.btnUploadFile.Location = new System.Drawing.Point(68, 382);
            this.btnUploadFile.Name = "btnUploadFile";
            this.btnUploadFile.Size = new System.Drawing.Size(75, 23);
            this.btnUploadFile.TabIndex = 3;
            this.btnUploadFile.Text = "Chọn file";
            this.btnUploadFile.UseVisualStyleBackColor = true;
            this.btnUploadFile.Click += new System.EventHandler(this.btnUploadFile_Click);
            // 
            // labelFileUploaded
            // 
            this.labelFileUploaded.Location = new System.Drawing.Point(68, 347);
            this.labelFileUploaded.Name = "labelFileUploaded";
            this.labelFileUploaded.ReadOnly = true;
            this.labelFileUploaded.Size = new System.Drawing.Size(318, 20);
            this.labelFileUploaded.TabIndex = 2;
            this.labelFileUploaded.TabStop = false;
            this.labelFileUploaded.Click += new System.EventHandler(this.labelFileUploaded_Click);
            // 
            // textPlainText
            // 
            this.textPlainText.Location = new System.Drawing.Point(68, 68);
            this.textPlainText.Name = "textPlainText";
            this.textPlainText.Size = new System.Drawing.Size(318, 186);
            this.textPlainText.TabIndex = 1;
            this.textPlainText.Text = "";
            this.textPlainText.Click += new System.EventHandler(this.textPlainText_Click);
            this.textPlainText.TextChanged += new System.EventHandler(this.textPlainText_TextChanged);
            // 
            // radioBtnFile
            // 
            this.radioBtnFile.AutoSize = true;
            this.radioBtnFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioBtnFile.Location = new System.Drawing.Point(23, 321);
            this.radioBtnFile.Name = "radioBtnFile";
            this.radioBtnFile.Size = new System.Drawing.Size(149, 20);
            this.radioBtnFile.TabIndex = 5;
            this.radioBtnFile.Text = "Chọn file cần băm";
            this.radioBtnFile.UseVisualStyleBackColor = true;
            this.radioBtnFile.Click += new System.EventHandler(this.radioBtnFile_Click);
            // 
            // radioBtnText
            // 
            this.radioBtnText.AutoSize = true;
            this.radioBtnText.Checked = true;
            this.radioBtnText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioBtnText.Location = new System.Drawing.Point(23, 42);
            this.radioBtnText.Name = "radioBtnText";
            this.radioBtnText.Size = new System.Drawing.Size(185, 20);
            this.radioBtnText.TabIndex = 0;
            this.radioBtnText.Text = "Nhập văn bản cần băm";
            this.radioBtnText.UseVisualStyleBackColor = true;
            this.radioBtnText.Click += new System.EventHandler(this.radioBtnText_Click);
            // 
            // btnHashMD5
            // 
            this.btnHashMD5.Enabled = false;
            this.btnHashMD5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHashMD5.Location = new System.Drawing.Point(425, 112);
            this.btnHashMD5.Name = "btnHashMD5";
            this.btnHashMD5.Size = new System.Drawing.Size(90, 69);
            this.btnHashMD5.TabIndex = 4;
            this.btnHashMD5.Text = "MD5";
            this.btnHashMD5.UseVisualStyleBackColor = true;
            this.btnHashMD5.Click += new System.EventHandler(this.btnHashMD5_Click);
            // 
            // btnHashSHA1
            // 
            this.btnHashSHA1.Enabled = false;
            this.btnHashSHA1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHashSHA1.Location = new System.Drawing.Point(425, 255);
            this.btnHashSHA1.Name = "btnHashSHA1";
            this.btnHashSHA1.Size = new System.Drawing.Size(90, 69);
            this.btnHashSHA1.TabIndex = 5;
            this.btnHashSHA1.Text = "SHA1";
            this.btnHashSHA1.UseVisualStyleBackColor = true;
            this.btnHashSHA1.Click += new System.EventHandler(this.btnHashSHA1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(555, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "Kết quả";
            // 
            // textHashResult
            // 
            this.textHashResult.Enabled = false;
            this.textHashResult.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textHashResult.Location = new System.Drawing.Point(558, 112);
            this.textHashResult.Name = "textHashResult";
            this.textHashResult.ReadOnly = true;
            this.textHashResult.Size = new System.Drawing.Size(414, 212);
            this.textHashResult.TabIndex = 9;
            this.textHashResult.TabStop = false;
            this.textHashResult.Text = "";
            // 
            // fileUpload
            // 
            this.fileUpload.FileName = "openFileDialog1";
            // 
            // buttonPaste
            // 
            this.buttonPaste.Location = new System.Drawing.Point(311, 260);
            this.buttonPaste.Name = "buttonPaste";
            this.buttonPaste.Size = new System.Drawing.Size(75, 23);
            this.buttonPaste.TabIndex = 2;
            this.buttonPaste.Text = "Dán";
            this.buttonPaste.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonPaste.UseVisualStyleBackColor = true;
            this.buttonPaste.Click += new System.EventHandler(this.buttonPaste_Click);
            // 
            // buttonCopy
            // 
            this.buttonCopy.Enabled = false;
            this.buttonCopy.Location = new System.Drawing.Point(897, 330);
            this.buttonCopy.Name = "buttonCopy";
            this.buttonCopy.Size = new System.Drawing.Size(75, 23);
            this.buttonCopy.TabIndex = 6;
            this.buttonCopy.Text = "Sao chép";
            this.buttonCopy.UseVisualStyleBackColor = true;
            this.buttonCopy.Click += new System.EventHandler(this.buttonCopy_Click);
            // 
            // Hash
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 461);
            this.Controls.Add(this.buttonCopy);
            this.Controls.Add(this.buttonPaste);
            this.Controls.Add(this.textHashResult);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnHashSHA1);
            this.Controls.Add(this.btnHashMD5);
            this.Controls.Add(this.radioBtnText);
            this.Controls.Add(this.radioBtnFile);
            this.Controls.Add(this.textPlainText);
            this.Controls.Add(this.labelFileUploaded);
            this.Controls.Add(this.btnUploadFile);
            this.Name = "Hash";
            this.Text = "Hash";
            this.Load += new System.EventHandler(this.Hash_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnUploadFile;
        private System.Windows.Forms.TextBox labelFileUploaded;
        private System.Windows.Forms.RichTextBox textPlainText;
        private System.Windows.Forms.RadioButton radioBtnFile;
        private System.Windows.Forms.RadioButton radioBtnText;
        private System.Windows.Forms.Button btnHashMD5;
        private System.Windows.Forms.Button btnHashSHA1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox textHashResult;
        private System.Windows.Forms.OpenFileDialog fileUpload;
        private System.Windows.Forms.Button buttonCopy;
        private System.Windows.Forms.Button buttonPaste;
    }
}

