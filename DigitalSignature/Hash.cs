﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace DigitalSignature
{
    public partial class Hash : Form
    {
        private string fileToHash;
        private const long maxLength = 4294967296; //max size of file that program should handle

        public Hash()
        {
            InitializeComponent();
        }

        private void Hash_Load(object sender, EventArgs e)
        {
            fileToHash = "";

            // enable the paste button if whether data in clipboard is text
            // no need to worry about radio since plaintext choice is selected by default
            if (Clipboard.ContainsText())
            {
                buttonPaste.Enabled = true;
            }
            else
            {
                buttonPaste.Enabled = false;
            }
        }

        private void btnUploadFile_Click(object sender, EventArgs e)
        {
            radioBtnFile.Checked = true;
            radioBtnText.Checked = false;

            OpenFileDialog uploadFile = new OpenFileDialog();
            uploadFile.Title = "Upload file to hash";
            uploadFile.InitialDirectory = @"c:\";
            uploadFile.Filter = "All files (*.*)|*.*|All files (*.*)|*.*";
            uploadFile.FilterIndex = 2;
            uploadFile.RestoreDirectory = true;
            if (uploadFile.ShowDialog() == DialogResult.OK) // upload result
            {
                fileToHash = uploadFile.FileName;
                labelFileUploaded.Text = fileToHash;
            }
            checkInputFile();          
        }

        private void labelFileUploaded_Click(object sender, EventArgs e)
        {
            radioBtnFile.Checked = true;
            radioBtnText.Checked = false;
            checkInputFile();
        }

        private void textPlainText_Click(object sender, EventArgs e)
        {
            radioBtnFile.Checked = false;
            radioBtnText.Checked = true;
            checkInputText();
        }

        private void btnHashMD5_Click(object sender, EventArgs e)
        {
            byte[] hashByte;
            using (HashAlgorithm hasher = MD5.Create())
            {
                // if choose hash a file
                if (radioBtnFile.Checked == true)
                {
                    try
                    {
                        //open file
                        using (var stream = File.OpenRead(fileToHash))
                        {
                            hashByte = hasher.ComputeHash(stream);
                        }
                        textHashResult.Text = MakeHashString(hashByte);
                        textHashResult.Enabled = true;
                        buttonCopy.Enabled = true;
                    }
                    catch
                    {
                        MessageBox.Show("Hãy chọn file hợp lệ để băm!", "Cảnh báo", MessageBoxButtons.OK);
                    }
                    
                }
                // if choose hash a text
                else
                {
                    if (textPlainText.Text != "")
                    {
                        // get ASCII code of plain text
                        hashByte = Encoding.UTF8.GetBytes(textPlainText.Text);
                        hashByte = hasher.ComputeHash(hashByte);
                        textHashResult.Text = MakeHashString(hashByte);
                        textHashResult.Enabled = true;
                        buttonCopy.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("Nhập ít nhất một ký tự để băm!", "Cảnh báo", MessageBoxButtons.OK);
                    }
                }
            }
            
        }

        private void btnHashSHA1_Click(object sender, EventArgs e)
        {
            byte[] hashByte;
            using (HashAlgorithm hasher = SHA1.Create())
            {
                // if choose hash a file
                if (radioBtnFile.Checked == true)
                {
                    try
                    {
                        //open file
                        using (var stream = File.OpenRead(fileToHash))
                        {
                            hashByte = hasher.ComputeHash(stream);
                        }
                        textHashResult.Text = MakeHashString(hashByte);
                        textHashResult.Enabled = true;
                        buttonCopy.Enabled = true;
                    }
                    catch
                    {
                        MessageBox.Show("Hãy chọn file hợp lệ để băm!", "Cảnh báo", MessageBoxButtons.OK);
                    }

                }
                // if choose hash a text
                else
                {
                    if (textPlainText.Text != "")
                    {
                        // get ASCII code of plain text
                        hashByte = Encoding.UTF8.GetBytes(textPlainText.Text);
                        hashByte = hasher.ComputeHash(hashByte);
                        textHashResult.Text = MakeHashString(hashByte);
                        textHashResult.Enabled = true;
                        buttonCopy.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("Nhập ít nhất một ký tự để băm!", "Cảnh báo", MessageBoxButtons.OK);
                    }
                }
            }
        }
        
        // bind a hash byte array to string
        private string MakeHashString(byte[] hashByte)
        {
            var sb = new StringBuilder();
            foreach (byte b in hashByte)
            {
                sb.Append(b.ToString("x2"));
            }

            return sb.ToString();
        }

        private void textPlainText_TextChanged(object sender, EventArgs e)
        {
            checkInputText();
        }

        // check if the input text field is empty  or not
        private void checkInputText()
        {
            if (textPlainText.Text != "")
            {
                btnHashMD5.Enabled = true;
                btnHashSHA1.Enabled = true;
            }
            else
            {
                btnHashMD5.Enabled = false;
                btnHashSHA1.Enabled = false;
            }

            // check the condition to enable the paste button of input plaintext area
            if (Clipboard.ContainsText())
            {
                buttonPaste.Enabled = true;
            }
            else
            {
                buttonPaste.Enabled = false;
            }
        }

        // check if the input file is valid or not
        private void checkInputFile()
        {
            // disable paste button of plaintext input area wherever choose input type is file
            buttonPaste.Enabled = false;

            if (labelFileUploaded.Text != "")
            {
                FileInfo fileInfo = new FileInfo(fileToHash);
                if (fileInfo.Length < maxLength)
                {
                    btnHashMD5.Enabled = true;
                    btnHashSHA1.Enabled = true;
                    return;
                }
            }
            btnHashMD5.Enabled = false;
            btnHashSHA1.Enabled = false;
        }

        private void radioBtnText_Click(object sender, EventArgs e)
        {
            checkInputText();
        }

        private void radioBtnFile_Click(object sender, EventArgs e)
        {
            checkInputFile();
        }
        

        private void buttonPaste_Click(object sender, EventArgs e)
        {
            textPlainText.Text += Clipboard.GetText();
        }

        private void buttonCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textHashResult.Text);
            MessageBox.Show("Đã sao chép vào bộ nhớ tạm", "Thông báo", MessageBoxButtons.OK);
        }
    }
}
