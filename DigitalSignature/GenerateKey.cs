﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitalSignature
{
    public partial class GenerateKey : Form
    {
        private BigInteger primeNo1 { get; set; }
        private BigInteger primeNo2 { get; set; }
        private BigInteger module { get; set; }
        private BigInteger totient { get; set; }
        private BigInteger coprime { get; set; } // combine with module => public key
        private BigInteger inverse { get; set; } // combine with module => private key

        public GenerateKey()
        {
            InitializeComponent();
        }

        private void btnNewKey_Click(object sender, EventArgs e)
        {
            // generate two distinct prime numbers
            ulong temp_primeNo1 = randomPrime();
            ulong temp_primeNo2;
            // we need to make sure primeNo1 <> primeNo2
            do
            {
                temp_primeNo2 = randomPrime();
            }
            while (temp_primeNo1 == temp_primeNo2);

            primeNo1 = new BigInteger(temp_primeNo1);
            primeNo2 = new BigInteger(temp_primeNo2);

            // compute module
            module = primeNo1 * primeNo2;

            // compute toient
            totient = (primeNo1 - 1) * (primeNo2 - 1);

            // find a coprime number with toient while 1 < coprime number < toient
            for (BigInteger i = 2; i < totient; i++)
            {
                if (GCD(i, totient) == 1)
                {
                    coprime = i;
                    break;
                }
            }
            // find inverse number of coprime a modulo toient
            inverse = MMI(coprime, totient);

            textEncryptKey.Text = coprime.ToString();
            textDecryptKey.Text = inverse.ToString();
            textModule.Text = module.ToString();
            textEncryptKey.Enabled = true;
            textDecryptKey.Enabled = true;
            textModule.Enabled = true;
            buttonCopyModule.Enabled = true;
            buttonCopyPublicKey.Enabled = true;
            buttonPrivateKey.Enabled = true;
        }

        // Check if input number is a prime number or not
        private bool isPrime(ulong number)
        {
            if (number == 2 || number == 3)
            {
                return true;
            }
            if (number == 1 || number % 2 == 0 || number % 3 == 0 || number % 5 == 0)
            {
                return false;
            }
            ulong rangeTest = (ulong)Math.Sqrt(number);
            for (ulong i = 5; i <= rangeTest; i++)
            {
                if (number % i == 0 || number % (i + 2) == 0)
                {
                    return false;
                }
            }
            return true;
        }

        // Generate a prime number (betwween 4444 and 4294967295)
        private ulong randomPrime()
        {
            do
            {
                ulong min = LongRandom(4444, uint.MaxValue, new Random());
                ulong max = LongRandom(4444, uint.MaxValue, new Random());
                if (min > max)
                {
                    ulong temp = min;
                    min = max;
                    max = temp;
                }
                for (ulong i = min; i <= max; i++)
                {
                    if (isPrime(i))
                    {
                        return i;
                    }
                }
            }
            while (true);
        }

        // Random a long int number
        private ulong LongRandom(ulong min, ulong max, Random rand)
        {
            byte[] buf = new byte[8];
            rand.NextBytes(buf);
            ulong longRand = BitConverter.ToUInt64(buf, 0);
            return ((longRand % (max - min)) + min);
        }

        // Find Greatest Common Divisor using Euclidean algorithm
        private int GCD(BigInteger n1, BigInteger n2)
        {
            while (n2 != 0)
            {
                BigInteger temp = n1 % n2;
                n1 = n2;
                n2 = temp;
            }

            return (int)n1;
        }

        // Find Modular Multiplicative Inverse of a number
        private BigInteger MMI(BigInteger number, BigInteger modulo)
        {
            BigInteger y0 = 0;
            BigInteger y1 = 1;
            BigInteger q = modulo / number;
            BigInteger y = y0 - y1 * q;
            BigInteger r = modulo % number;
            BigInteger negative = modulo;

            while (number > 0)
            {
                modulo = number;
                number = r;
                y0 = y1;
                y1 = y;
                r = modulo % number;
                if (r == 0)
                {
                    break;
                }
                q = modulo / number;
                y = y0 - y1 * q;
            }

            if (number > 1)
                return 0;

            if (y < 0)
                y += negative;
            return y;
        }

        private void buttonCopyPublicKey_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textEncryptKey.Text);
            MessageBox.Show("Đã sao chép khóa công khai vào bộ nhớ đệm", "Thông báo", MessageBoxButtons.OK);
        }

        private void buttonPrivateKey_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textDecryptKey.Text);
            MessageBox.Show("Đã sao chép khóa bí mật vào bộ nhớ đệm", "Thông báo", MessageBoxButtons.OK);
        }

        private void buttonCopyModule_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textModule.Text);
            MessageBox.Show("Đã sao chép module mã hóa vào bộ nhớ đệm", "Thông báo", MessageBoxButtons.OK);
        }
    }
}
